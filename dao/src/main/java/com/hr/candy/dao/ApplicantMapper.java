package com.hr.candy.dao;

import com.hr.candy.dao.model.ApplicantPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ApplicantMapper {

    List<ApplicantPO> queryAll();
}
