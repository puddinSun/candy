package com.hr.candy.dao;

import com.hr.candy.dao.model.AppointmentPO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AppointmentMapper {

    List<AppointmentPO> queryAll();
}
