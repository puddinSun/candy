package com.hr.candy.dao.model;

import lombok.Getter;

@Getter
public class AppointmentPO {

    Integer id;
    String appointmentDate;
}
