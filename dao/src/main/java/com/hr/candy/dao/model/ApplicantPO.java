package com.hr.candy.dao.model;

import lombok.Getter;

@Getter
public class ApplicantPO {

    Integer id;
    String applicantName;
}
