package com.hr.candy.web.controller;

import com.hr.candy.applicant.service.ApplicantService;
import com.hr.candy.appointment.service.AppointmentService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BizController {

    @Autowired
    AppointmentService appointmentService;

    @Autowired
    ApplicantService applicantService;

    @Value("${candy.web.message}")
    String message;

    @GetMapping("/appointments")
    public String getAppointment() {
        return JSONObject.valueToString(appointmentService.viewAppointment());
    }

    @GetMapping("/applicants")
    public String getApplicants() {
        return JSONObject.valueToString(applicantService.queryAll());
    }

    @GetMapping("/message")
    public String sayHi() {
        return message;
    }

}
