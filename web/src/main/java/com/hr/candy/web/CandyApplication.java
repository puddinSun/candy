package com.hr.candy.web;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(scanBasePackages = "com.hr.candy")
@MapperScan(basePackages = "com.hr.candy.dao")
public class CandyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CandyApplication.class, args);
    }

    @GetMapping("/hello")
    public String method() {
        return "Hello multi module";
    }
}
