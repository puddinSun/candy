package com.hr.candy.applicant.service;

import com.hr.candy.dao.ApplicantMapper;
import com.hr.candy.dao.model.ApplicantPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApplicantService {

    @Autowired
    ApplicantMapper applicantMapper;

    public List<ApplicantPO> queryAll() {
        return applicantMapper.queryAll();
    }
}
