package com.hr.candy.appointment.service;

import com.hr.candy.dao.AppointmentMapper;
import com.hr.candy.dao.model.AppointmentPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentService {

    @Autowired
    AppointmentMapper appointmentMapper;

    public List<AppointmentPO> viewAppointment() {
        return appointmentMapper.queryAll();
    }

}
